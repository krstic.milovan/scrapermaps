const config = require("./src/config");

const knexConfig = {
	client: "pg",
	connection: config.get("POSTGRES_URL"),
	migrations: {
		directory: "./migrations"
	}
};

module.exports = {
	production: knexConfig,
	staging: knexConfig,
	development: knexConfig
};
