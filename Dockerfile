FROM node:14


COPY package*.json ./

RUN yarn

COPY . ./app

EXPOSE 5000

CMD [ "yarn", "watch" ]
