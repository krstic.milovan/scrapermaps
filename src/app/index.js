const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const sentry = require("@sentry/node");

const config = require("../config");

const app = express();

app.use(bodyParser.json({limit: "50mb"}));
app.use(cors());
app.use(morgan("combined"));

sentry.init({
	dsn: config.get("SENTRY_DSN"),
	integrations: [
		new sentry.Integrations.Http({ tracing: true }),
		new sentry.Integrations.Express({ app }),
	],
	tracesSampler: 1.0
});



app.use("/api/v1", require("../api/routes"));

module.exports = app;
