const db = require("../utils/db/knex.js");

const cyrillicToLatin = require("cyrillic-to-latin");
var unique = require("array-unique").immutable;

module.exports = (trx=db) => {
	const store = require("./store")(trx);
	const add = (leads) => {

		const correlations = leads.flatMap(lead => {
			const { categories, lead_id } = lead;

			return categories.map(category => {
				return {
					lead_id,
					category: (cyrillicToLatin(category)).toLowerCase()
				};
			});
		});

		const categories = unique(correlations.map(corr => corr.category))
			.map(cat => {
				return {
					category: cat
				};
			});

		return store.insert(categories)
			.then(() => {
				return db("place_tags").insert(correlations);
			});
	};
	const getAllCategorys = () => {
		return store.get();
	};

	return {
		add,
		getAllCategorys
	};

};
