const db = require("../utils/db/knex.js");

module.exports = (trx=db) => {
	const Categories = () => trx("categories");
	const get = (qurey) => Categories().where(qurey);
	const insert = (categories) => Categories().insert(categories).onConflict().ignore().returning("*").then(res => res[0]);
	const update = (qurey, set) => Categories().where(qurey).update(set).returning("*").then(res => res[0]);

	return {
		get,
		insert,
		update,
	};
};
