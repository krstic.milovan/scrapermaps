const config = require("./config");
const app = require("./app");
const port = config.get("PORT");


app.listen(port, () => {
	console.log(`[🚀] Running ${config.get("APP_NAME")} on the ${config.get("PORT")} port.`);
});
