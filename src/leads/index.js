const { lookUpRaw } = require("geojson-places");

const db = require("../utils/db/knex.js");
const fsWrapper = require("../utils/fs")();

const leadSchema = {
	lead_id: true,
	geoloc: true,
	address: true,
	name: true,
	phone: true,
	reviews: true,
	rating: true,
	main_category: true,
	website: true
};

const getPlace = (leadGeoloc) => {
	const geoloc = leadGeoloc.split(",").map(loc => parseFloat(loc));
	const place = lookUpRaw(geoloc[0], geoloc[1]);
	const properties = place.features[0].properties;
	return `${properties.geonunit}-${properties.name_en}`;
};

const formatLeads =(leads) => leads.map(lead => {
	const filteredLead = {};
	for (const key in lead) {
		if (leadSchema[key]) {
			if(key==="geoloc"){
				filteredLead["place"] = getPlace(lead[key]);
			}

			filteredLead[key] = lead[key];
		}
	}
	return filteredLead;
});

module.exports = (trx=db) => {
	const store = require("./store")(trx);

	const add = (leads) => {
		const fleads = formatLeads(leads);
		return store.insert(fleads);
	};

	const getOne = (id) => {
		const query = {id};
		return store.getOne(query);
	};

	const getLeadsForAccount = (account_id, include_deleted = false, filters= {}, limit, offset, sort) => {
		const query = {
			account_id,
			is_deleted: include_deleted
		};

		const queryDb =  db("associations")
			.select("leads.*", db.raw("ARRAY_AGG(DISTINCT place_tags.category) AS categories"))
			.leftJoin("leads", "leads.lead_id", "associations.lead_id")
			.leftJoin("place_tags", "place_tags.lead_id", "leads.lead_id");

		console.log(filters.categories);
		if(filters.categories) {
			queryDb.whereIn("place_tags.category", filters.categories);
		}

		if(filters.place_name) {
			queryDb.where("leads.place", "like", `%${filters.place_name}%`);
		}

		queryDb.whereNotNull("leads.phone");
		queryDb.where(query);
		queryDb.groupBy("leads.lead_id");
		queryDb.orderBy(`leads.${sort.orderBy}`, sort.sortDirection);

		if(limit){
			queryDb.limit(limit);
		}
		return queryDb.offset(offset);
	};

	const seen = (lead_id) => {
		const query = {
			lead_id
		};

		const set = {
			seen: true
		};

		return  store.update(query, set);
	};


	return {
		add,
		getOne,
		getLeadsForAccount,
		seen
	};

};

