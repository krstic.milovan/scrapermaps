const db = require("../utils/db/knex.js");

module.exports = (trx=db) => {
	const Leads = () => trx("leads");
	const get = (qurey) => Leads().where(qurey);
	const getOne = (qurey) => get(qurey).first();
	const insert = (leads) => Leads().insert(leads).onConflict().ignore().returning("*").then(res => res[0]);
	const update = (qurey, set) => Leads().where(qurey).update(set).returning("*").then(res => res[0]);

	return {
		get,
		getOne,
		insert,
		update,
	};
};
