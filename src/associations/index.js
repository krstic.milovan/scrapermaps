const db = require("../utils/db/knex.js");

module.exports = (trx=db) => {
	const store = require("./store")(trx);

	const add = (leads, account_id) => {
		const leadids = leads.map(lead => {
			return {
				lead_id: lead.lead_id,
				account_id: account_id
			};
		});

		return store.insert(leadids);
	};

	const remove = (lead_id, account_id) => {
		const qurey = {
			lead_id,
			account_id
		};
		const set = {
			is_deleted : true
		};
		return store.remove(qurey, set);
	};

	return {
		add,
		remove
	};
};
