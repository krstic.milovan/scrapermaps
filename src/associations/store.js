const db = require("../utils/db/knex.js");

module.exports = (trx=db) => {
	const Associations = () => trx("associations");
	const get =  (query) => Associations().where(query);
	const insert = (assocs) => Associations().insert(assocs).onConflict().ignore().returning("*").then(res => res[0]);
	const remove = (qurey, set) => Associations().where(qurey).update(set).returning("*").then(res => res[0]);

	return {
		get,
		insert,
		remove
	};
};
