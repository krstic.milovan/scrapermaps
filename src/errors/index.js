const uuid = require("uuid");
const globalServiceName = "Scrapper service";


module.exports = {
	deletingLead: (lead_id) => {
		return {
			id: uuid.v4(),
			service_name: globalServiceName,
			status: 500,
			type: "InternalServerError",
			message: `failed to delete lead with id: ${lead_id}`
		};
	},
	updatingLead: (lead_id) => {
		return {
			id: uuid.v4(),
			service_name: globalServiceName,
			status: 500,
			type: "InternalServerError",
			message: `failed to update lead with id: ${lead_id}`
		};
	},
	ValidationError: (message) => {
		return {
			id: uuid.v4(),
			service_name: globalServiceName,
			status: 422,
			type: "Bad Request",
			message: message
		};
	},
	CsvError: () => {
		return {
			id: uuid.v4(),
			service_name: globalServiceName,
			status: 500,
			type: "InternalServerError",
			message: "failed to make a csv file"
		};

	}
};
