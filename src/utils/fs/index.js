const csv = require("csv-writer").createObjectCsvStringifier;

module.exports = () => {

	const makeLeadCSV = () => {
		const csvStringifier = csv({
			header: [
				{ id: "address", title: "address" },
				{ id: "phone", title: "phone" },
				{ id: "place", title: "place" },
				{ id: "name", title: "name" },
				{ id: "categories", title: "categories"}
			]
		});

		return csvStringifier;
	};

	return {
		makeLeadCSV,
	};
};
