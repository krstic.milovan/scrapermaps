const admin = require("firebase-admin");

const config = require("../../../config");

admin.initializeApp({
	credential: admin.credential.cert(config.get("FIREBASE_ADMIN_KEY"))
});



module.exports = {
	verify: (token) => admin.auth().verifyIdToken(token)
};
