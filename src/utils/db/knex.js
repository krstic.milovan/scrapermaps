const config = require("../../config");

const db = require("knex")({
	client: "pg",
	connection: config.get("POSTGRES_URL"),
	pool: { min: 0, max: 7 }
});

module.exports = db;
