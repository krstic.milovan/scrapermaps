const admin = require("../../../utils/firebase/admin");

function auth (req, res, next) {
	const token = getToken(req.headers);

	admin.verify( token || "").then(user => {
		req.user = user;
		next();
	}).catch(() => {
		return res.status(401)
			.json({
				status: "error",
				error: {
					message: "Authorization Error",
					type: "AuthorizationError"
				}
			});
	});
}

function getToken (headers) {
	if (headers && headers.authorization) {
		const parted = headers.authorization.split(" ");
		if (parted.length === 2) {
			return parted[1];
		} else {
			return null;
		}

	} else {
		return null;
	}
}

module.exports = auth;
