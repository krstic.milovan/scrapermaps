module.exports = (error, _req, res, _next) => {
	if (!_next) {
		console.log(_next);
	}
	if (error.status) {
		return res.status(error.status)
			.json({
				status: "fail",
				error
			});
	}

	let errorMessage = "Unknown Error";

	let status = 500;

	if (error && error.message) {
		console.error(error.message);
	} else {
		console.error(error);
	}

	if (error && error.error && error.error.code) {
		errorMessage = error.error.message || errorMessage;
		status = error.error.code;
	}

	if (error && error.response && error.response.statusCode) {
		errorMessage = error.message || errorMessage;
		status = error.response.statusCode;

		if (status == 200) {
			status = 500;
		}
	}

	return res.status(status)
		.json({
			status: "fail",
			error: {
				message: errorMessage,
				type: "UnknownClientError",
				code: status
			}
		});
};
