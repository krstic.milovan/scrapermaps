const boom = require("@hapi/boom");
const errors = require("../../../errors");

module.exports = (schema) => {
	return (req, res, next) => {

		if(req.query.categories){
			req.query.categories = JSON.parse(req.query.categories);
		}

		schema.validateAsync(
			req,
			{
				allowUnknown: true,
				stripUnknown: true
			}
		).then((value) => {

			req.body = value.body;
			req.params = value.params;
			req.query = value.query;

			next();
		}).catch(error => {
			const output = (boom.badRequest(error, "ValidationError")).output;
			const message = output.payload.message;
			next(errors.ValidationError(message));
		});
	};
};
