const Joi = require("joi");

const leadSchema = Joi.array().items(
	Joi.object({
		lead_id: Joi.string().required(),
		geoloc: Joi.string().required(),
		name: Joi.string().required(),
		main_category: Joi.string(),
		rating: Joi.any(),
		website: Joi.any(),
		phone: Joi.any().default("no number").required(),
		address: Joi.any(),
		categories: Joi.array().items(Joi.string()).default(["uncategorized"]).min(1).required(),
	})
).min(1).required();


module.exports = Joi.object({
	body: {
		leads: leadSchema
	}
});
