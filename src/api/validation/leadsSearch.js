const Joi = require("joi");

const categoriesSchema = Joi.array().items(Joi.string());
const placeSchema = Joi.string();
const deletedSchema = Joi.boolean().default(false);
const limitSchema = Joi.number().integer().default(30);
const offsetScema = Joi.number().integer().default(0);
const sortSchema = Joi.object({
	orderBy: Joi.string(),
	sortDirection: Joi.string()
}).default({
	orderBy: "name",
	sortDirection: "asc"
});

module.exports = Joi.object({
	query: {
		categories: categoriesSchema,
		place_name: placeSchema,
		include_deleted: deletedSchema,
		limit: limitSchema,
		offset: offsetScema,
		sort: sortSchema
	}
});
