const express = require("express");
const sentry = require("@sentry/node");

const router =  express.Router();

router.use(sentry.Handlers.requestHandler());
router.use(sentry.Handlers.tracingHandler());


const error =  require("../middleware/error");
const auth =  require("../middleware/auth");

router.use("/health", require("./health"));

router.use(auth);

router.use("/scrapper/leads", require("./leads"));

router.use(error);

module.exports = router;
