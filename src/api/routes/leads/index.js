const express =  require("express");

const Leads = require("../../../leads")();
const Categories = require("../../../categories")();
const Associations = require("../../../associations")();

const validator =  require("../../middleware/validation");
const errors = require("../../../errors");

const leadsSearch = require("../../validation/leadsSearch.js");
const leadInsert = require("../../validation/leadsInsert");
const leadsDownload = require("../../validation/leadsDownload");

const csv = require("../../../utils/fs/")().makeLeadCSV();

const router = express.Router();

router.get("/", validator(leadsSearch), (req, res, next) => {
	const account_id = req.user.sub;
	const include_deleted = req.query.include_deleted;
	const filters = {
		categories: req.query.categories,
		place_name: req.query.place_name
	};
	const limit = req.query.limit;
	const offset =  req.query.offset;
	const sort = req.query.sort;

	Leads.getLeadsForAccount(account_id, include_deleted, filters, limit, offset, sort)
		.then(leads => {
			res.status(200)
				.json({
					status: "ok",
					leads,
				});
		})
		.catch(error => next(error));
});


router.post("/", validator(leadInsert) ,(req, res, next) => {
	const leads =  req.body.leads;
	const account_id = req.user.sub;


	const chain = (async () => {
		await Leads.add(leads);
		await Categories.add(leads);
		await Associations.add(leads, account_id);
	})();

	return chain.then(() => {
		return res.status(200).json({
			status: "ok"
		});
	}).catch(error => next(error));
});


router.delete("/:lead_id", (req, res, next) => {
	const lead_id = req.params.lead_id;
	const account_id = req.user.sub;
	return Associations.remove(lead_id, account_id)
		.then((lead) => {
			if(lead) {
				res.status(200)
					.json({
						status: "ok"
					});
			}else {
				next(errors.deletingLead(lead_id));
			}
		}).catch(error => next(error));
});

router.patch("/:lead_id", (req, res, next) => {
	const lead_id = req.params.lead_id;
	return Leads.seen(lead_id)
		.then((lead) => {
			if(lead) {
				res.status(200)
					.json({
						status: "ok"
					});
			}else {
				next(errors.updatingLead(lead_id));
			}
		}).catch(error => next(error));
});


router.get("/csv", validator(leadsDownload), (req, res, next) => {
	const account_id = req.user.sub;
	const filters = {
		categories: req.query.categories,
		place_name: req.query.place_name
	};
	const sort = req.query.sort;

	Leads.getLeadsForAccount(account_id, false, filters, null, 0, sort)
		.then(leads => {
			try {
				res.setHeader("Content-Type", "text/csv");
				res.setHeader("Content-Disposition", "attachment; filename='data.csv'");

				res.write(csv.getHeaderString());
				res.write(csv.stringifyRecords(leads));
				res.end();

			}catch (error) {
				next(errors.csv);
			}
		}).catch(error => next(error));

});
module.exports = router;
