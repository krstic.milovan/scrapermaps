exports.up = function(knex) {
	return knex.schema.createTable("categories", function(table) {
		table.string("category").primary();
	});
};

exports.down = function(knex) {
	return knex.schema.dropTableIfExists("categories");
};

