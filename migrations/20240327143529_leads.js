exports.up = function(knex) {
	return knex.schema.createTable("leads", function(table) {
		table.string("lead_id").primary();
		table.string("geoloc");
		table.string("place");
		table.string("name");
		table.string("main_category").defaultTo("no main categories");
		table.decimal("rating", 3, 1).defaultTo(0.0);
		table.string("website").defaultTo("no website");
		table.string("phone");
		table.string("address").defaultTo("no address");
		table.boolean("seen").defaultTo(false);
		table.timestamp("created_at").defaultTo(knex.fn.now()).notNullable();
	});
};

exports.down = function(knex) {
	return knex.schema.dropTableIfExists("leads");
};
