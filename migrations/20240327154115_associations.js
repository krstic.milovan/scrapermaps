exports.up = function(knex) {
	return knex.schema.createTable("associations", function(table) {
		table.string("account_id");
		table.string("lead_id");
		table.boolean("is_deleted").defaultTo(false);
		table.foreign("lead_id").references("lead_id").inTable("leads");
	});
};

exports.down = function(knex) {
	return knex.schema.dropTable("associations");
};


