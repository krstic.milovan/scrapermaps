exports.up = function(knex) {
	return knex.schema.createTable("commnets", function(table) {
		table.string("account_id");
		table.string("lead_id");
		table.string("commnet", 256);
		table.foreign("lead_id").references("lead_id").inTable("leads");
	});
};

exports.down = function(knex) {
	return knex.schema.dropTable("commnets");
};


