exports.up = function(knex) {
	return knex.schema.createTable("place_tags", function(table) {
		table.uuid("id").defaultTo(knex.raw("uuid_generate_v4()")).primary();
		table.string("lead_id");
		table.string("category");
		table.index(["lead_id", "category"]);
		table.foreign("lead_id").references("lead_id").inTable("leads");
		table.foreign("category").references("category").inTable("categories");
	});
};

exports.down = function(knex) {
	return knex.schema.dropTableIfExists("place_tags");
};

